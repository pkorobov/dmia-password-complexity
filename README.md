# DMIA ML in Production: Password Complexity

This repo contains a simple symbol-level LSTM regression model, solving a task from [this competition](https://www.kaggle.com/c/dmia-production-ml-2021-1-passwords). \
The task is to predict how easily a password could be guessed.

The model is wrapped into a web app and can be tested here: http://dmia-passwords.link:5000. \
Actual weights of the server model can be found in `char_lstm_model/weights` file.

You can retrain the model by running `train.py` script, the result weights will be at `char_lstm_model/trained_weights`
