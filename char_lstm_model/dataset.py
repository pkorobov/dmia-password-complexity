import torch
from torch.utils.data import Dataset, DataLoader
from torch.nn.utils.rnn import pad_sequence
from .helpers import tokenize, char_to_idx


class PasswordDataset(Dataset):

    def __init__(self, passwords, targets=None):
        self.tokens = list(map(tokenize, passwords))
        self.targets = targets

    def __getitem__(self, idx):
        tokens = self.tokens[idx]
        target = self.targets[idx] if self.targets is not None else None
        if target is None:
            return tokens
        return tokens, target

    def __len__(self):
        return len(self.tokens)


class PaddingCollator:
    def __init__(self, pad_token_id, batch_first=False, no_targets=False):
        self.pad_token_idx = pad_token_id
        self.batch_first = batch_first
        self.no_targets = no_targets

    def __call__(self, batch):

        if self.no_targets:
            x = pad_sequence([entry for entry in batch], padding_value=self.pad_token_idx, batch_first=self.batch_first)
            x_lengths = torch.tensor([entry.shape[0] for entry in batch])
            return x, x_lengths
        else:
            x = pad_sequence([pair[0] for pair in batch], padding_value=self.pad_token_idx,
                             batch_first=self.batch_first)
            x_lengths = torch.tensor([pair[0].shape[0] for pair in batch])
            targets = torch.tensor([pair[1] for pair in batch])
            return x, x_lengths, targets


def create_loader(X, y, batch_size=128, shuffle=True):

    dataset = PasswordDataset(X, y)
    loader = DataLoader(
        dataset,
        batch_size=batch_size,
        shuffle=shuffle,
        collate_fn=PaddingCollator(
            pad_token_id=char_to_idx['<PAD>'],
            batch_first=False,
        )
    )
    return loader
