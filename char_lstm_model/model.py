import torch
import torch.nn as nn
from torch.optim import Adam
from torch.nn.utils import clip_grad_norm_
from torch.utils.tensorboard import SummaryWriter
from .helpers import tokenize, fix_seed
from .dataset import create_loader
import shutil


class WrappedModel:
    def __init__(self, n_characters, hidden_size, output_size, padding_idx, n_layers, learning_rate=1e-3,
                 batch_size=128, num_epochs=3, shuffle_train=True, clip_grad_value=100, seed=None,
                 log_dir="logs", device=torch.device("cpu"), initial_weights_path=None):

        self.model = CharLSTM(n_characters, hidden_size, output_size, padding_idx, n_layers).to(device)
        self.learning_rate = learning_rate
        self.batch_size = batch_size
        self.shuffle_train = shuffle_train
        self.num_epochs = num_epochs
        self.clip_grad_value = clip_grad_value
        self.device = device
        self.log_dir = log_dir
        self.seed = seed

        if seed is not None:
            fix_seed(seed)

        if initial_weights_path is not None:
            self.model.load_state_dict(torch.load(initial_weights_path, map_location=self.device))

    def fit(self, X, y):

        optimizer = Adam(self.model.parameters(), lr=self.learning_rate)
        criterion = nn.MSELoss()
        loader = create_loader(X, y, self.batch_size, self.shuffle_train)
        self.model.train()

        shutil.rmtree(self.log_dir)
        writer = SummaryWriter(self.log_dir)
        step = 0

        for epoch in range(self.num_epochs):

            running_loss = 0
            batch_count = 0

            for batch_x, batch_x_lengths, batch_y in loader:
                batch_x = batch_x.to(self.device)
                batch_x_lengths = batch_x_lengths.to(self.device)
                batch_y = batch_y.to(self.device)
                batch_pred = self.model(batch_x, batch_x_lengths).squeeze()

                optimizer.zero_grad()
                loss = criterion(batch_pred, batch_y)
                loss.backward()
                optimizer.step()

                grad_norm = clip_grad_norm_(self.model.parameters(), self.clip_grad_value)
                writer.add_scalar("train/batch_loss", loss.sqrt(), step)
                writer.add_scalar("train/grad_norm", grad_norm, step)

                running_loss += loss.item()
                batch_count += 1

                step += 1

            writer.add_scalar("train/epoch_loss", (running_loss / batch_count) ** 0.5, epoch)
        torch.save(self.model.state_dict(), "./char_lstm_model/trained_weights")

    def predict(self, string):
        tensor = tokenize(string).reshape(-1, 1)
        length = torch.LongTensor([len(string)])
        with torch.no_grad():
            return self.model(tensor, length).expm1().item()


class CharLSTM(nn.Module):
    def __init__(self, input_size, hidden_size, output_size, padding_idx=None, n_layers=1):
        super(CharLSTM, self).__init__()
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.output_size = output_size
        self.n_layers = n_layers
        self.padding_idx = padding_idx

        self.embedding = nn.Embedding(input_size, hidden_size, padding_idx=padding_idx)
        self.lstm = nn.LSTM(hidden_size, hidden_size, n_layers)
        self.dense = nn.Linear(hidden_size, output_size)

    def forward(self, x, x_lengths):
        x = self.embedding(x)
        x = torch.nn.utils.rnn.pack_padded_sequence(x, x_lengths.cpu(), enforce_sorted=False)
        h, _ = self.lstm(x)
        h, _ = torch.nn.utils.rnn.pad_packed_sequence(h, padding_value=self.padding_idx)

        device = next(self.parameters()).device
        _, batch_size, _ = h.shape

        h_last = h[x_lengths - 1, torch.arange(0, batch_size, device=device), :]
        output = self.dense(h_last.view(batch_size, -1))

        return output

    def validate(self, val_loader, device=torch.device("cuda")):
    
        result = torch.Tensor([]).reshape(0)
        with torch.no_grad():
            for batch_x in val_loader:

                batch_x = batch_x.to(device)
                batch_pred = self.forward(batch_x).squeeze()
                result.cat([result, batch_pred])

        return result
