import string
import torch
import numpy as np
import random

all_characters = string.printable
char_to_idx = dict(zip(all_characters, range(len(all_characters))))
char_to_idx["<PAD>"] = len(char_to_idx)
n_characters = len(char_to_idx)


def tokenize(string):
    tensor = torch.zeros(len(string)).long()
    for c in range(len(string)):
        try:
            tensor[c] = all_characters.index(string[c])
        except ValueError:
            continue
    return tensor


def validate(model, val_loader, device=torch.device("cpu")):

    running_loss, n = 0.0, 0
    with torch.no_grad():

        for batch_x, batch_x_lengths, batch_y in val_loader:
            batch_x = batch_x.to(device)
            batch_x_lengths = batch_x_lengths.to(device)
            batch_y = batch_y.to(device)

            batch_pred = model(batch_x, batch_x_lengths).squeeze()
            loss = (batch_pred - batch_y).pow(2).sum()
            running_loss += loss.item()
            n += batch_x.shape[1]

    rmse = np.sqrt(running_loss / n)
    return rmse


def fix_seed(seed):
    np.random.seed(seed)
    torch.manual_seed(seed)
    random.seed(seed)
    if torch.cuda.is_available():
        torch.cuda.manual_seed_all(seed)
        torch.backends.cudnn.deterministic = True
        torch.backends.cudnn.benchmark = False
