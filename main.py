from flask import Flask, render_template, request
from char_lstm_model.model import WrappedModel
import json

with open("char_lstm_model/config.json", "r") as config_file:
    config = json.load(config_file)
model = WrappedModel(**config)
app = Flask(__name__)


@app.route("/", methods=["POST", "GET"])
def index():
    """Main form rendering"""
    if request.method == 'POST':
        pw = request.form["password"]
        if len(pw) == 0:
            return render_template("index.html", password="Password should not be empty!", prediction="")
        pass_freq = round(model.predict(pw), 4)
        category = "Strong" if pass_freq < 2 else "Weak"
        return render_template("index.html", password=pw, prediction=f"{pass_freq} ({category})")
    else:
        return render_template("index.html")


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, threaded=False, debug=True)