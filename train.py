import pandas as pd
import numpy as np
from char_lstm_model.model import WrappedModel
import json

data = pd.read_csv("data/train.csv")
data = data.dropna()

max_seq_len = data.Password.apply(len).quantile(0.98)
data["Password"] = data.Password.astype(str).apply(lambda x: x[:max_seq_len])
data["Times"] = np.log1p(data.Times).astype(np.float32)

with open("char_lstm_model/config.json", "r") as config_file:
    config = json.load(config_file)

model = WrappedModel(**config, num_epochs=3, seed=24)
model.fit(data.Password.values, data.Times.values)
